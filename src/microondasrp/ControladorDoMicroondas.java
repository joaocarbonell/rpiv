package microondasrp;

import java.util.Objects;

public class ControladorDoMicroondas implements IControlador {

    private Microondas microondas;
    private Display display;

    private static ControladorDoMicroondas instancia = null;

    private ControladorDoMicroondas() {
        this.microondas = new Microondas();
        this.display = new Display();

        this.microondas.start();
    }

    public static ControladorDoMicroondas getInstancia() {
        if (instancia == null) {
            instancia = new ControladorDoMicroondas();
        }
        return instancia;
    }

    public static void deleteInstancia() {
        instancia = null;
    }

    @Override
    public Microondas getMicroondas() {
        return this.microondas;
    }

    @Override
    public void adicionarTempoDeCozimento(int tempo) {
        int minutos = 0, segundos = 0;

        int novoTempo = microondas.getTempo() + tempo;

        minutos = novoTempo / 60;
        segundos = novoTempo % 60;

        microondas.setTempo(novoTempo);
        display.ajustarTempoExibido(minutos, segundos);
        ControladorDeSom.getInstancia().reproduzir("beep");

    }

    @Override
    public void reduzirTempoDeCozimento(int tempo) {
        int minutos = 0, segundos = 0;

        int novoTempo = microondas.getTempo() - tempo;

        minutos = novoTempo / 60;
        segundos = novoTempo % 60;

        microondas.setTempo(novoTempo);
        display.ajustarTempoExibido(minutos, segundos);
        ControladorDeSom.getInstancia().reproduzir("beep");

    }

    @Override
    public void travarPainel() {
        microondas.travar();

        desabilitarAbrirEFecharPorta();
        ControladorDeSom.getInstancia().reproduzir("beep");

    }

    @Override
    public void destravarPainel() {
        microondas.destravar();
        ControladorDeSom.getInstancia().reproduzir("beep");

        if (!microondas.isLigado()) {
            reabilitarAbrirEFecharPorta();

        }
    }

    @Override
    public void ligarCozimento() {
        if (!microondas.isPortaAberta()) {
            microondas.ligar();
            display.iniciarCozimento();
            ControladorDeSom.getInstancia().reproduzir("beep");
            desabilitarAbrirEFecharPorta();
        }
    }

    @Override
    public void desligarCozimento() {
        if (microondas.isLigado()) {
            microondas.desligar();
            display.pararCozimento();
            ControladorDeSom.getInstancia().reproduzir("beep");

            reabilitarAbrirEFecharPorta();
        }
    }

    @Override
    public void abrirPorta() {
        if (!microondas.isLigado()) {
            microondas.abrirPorta();
            display.abrirPorta();
            ControladorDeSom.getInstancia().reproduzir("beep");

        }
    }

    @Override
    public void fecharPorta() {
        if (!microondas.isLigado()) {
            microondas.fecharPorta();
            display.fecharPorta();
            ControladorDeSom.getInstancia().reproduzir("beep");

        }
    }

    @Override
    public void escolherPotencia(int potencia) {
        microondas.setPotencia(potencia);
        switch (potencia) {
            case Microondas.POTENCIA_ALTA:
                display.marcarPotenciaAlta();
                ControladorDeSom.getInstancia().reproduzir("beep");

                break;
            case Microondas.POTENCIA_MEDIA:
                display.marcarPotenciaMedia();
                ControladorDeSom.getInstancia().reproduzir("beep");

                break;
            case Microondas.POTENCIA_BAIXA:
                display.marcarPotenciaBaixa();
                ControladorDeSom.getInstancia().reproduzir("beep");

                break;
        }
    }

    /**
     * Não sabemos se é possivel testar
     */
    private void desabilitarAbrirEFecharPorta() {
        display.desabilitarAbrirEFecharPorta();
        ControladorDeSom.getInstancia().reproduzir("beep");

    }

    private void reabilitarAbrirEFecharPorta() {
        display.reabilitarAbrirEFecharPorta();
        ControladorDeSom.getInstancia().reproduzir("beep");

    }

}
