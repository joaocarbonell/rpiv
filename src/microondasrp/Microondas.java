package microondasrp;

public class Microondas extends Thread {

    private final int UM_SEGUNDO = 1000; //1000 milissegundos

    public static final int POTENCIA_ALTA = 1;
    public static final int POTENCIA_MEDIA = 2;
    public static final int POTENCIA_BAIXA = 3;

    private long tempoDaUltimaAtualizacao;
    private int tempo;
    private int potencia;
    private boolean portaAberta;
    private boolean travado;
    private boolean ligado; //cozinhando

    public Microondas() {
        this.tempoDaUltimaAtualizacao = System.currentTimeMillis();
        this.tempo = 0;
        this.potencia = POTENCIA_ALTA;
        this.portaAberta = false;
        this.travado = false;
        this.ligado = false;
    }

    /* método que faz um microondas executar independentemente */
    @Override
    public void run() {
        /* laço de execução contínua */
        while (true) {
            long tempoAtual = System.currentTimeMillis();
            /* se ainda não acabou o tempo e está cozinhando */
            if ((getTempo() != 0) && isLigado()) {
                /* se já passou um segundo */
                if (tempoAtual - tempoDaUltimaAtualizacao > UM_SEGUNDO) {
                    ControladorDoMicroondas.getInstancia().reduzirTempoDeCozimento(1);
                    
                    
                    tempoDaUltimaAtualizacao = System.currentTimeMillis();
                } else {
                    /* se ainda não passou um segundo */
                    //NÃO FAZER NADA
                }
            }
        }
    }

    public int getTempo() {
        return this.tempo;
    }

    public void setTempo(int tempo) {
        this.tempo = tempo;
    }

    public int getPotencia() {
        return this.potencia;
    }

    public void setPotencia(int potencia) {
        this.potencia = potencia;
    }

    public boolean isPortaAberta() {
        return this.portaAberta;
    }

    public void abrirPorta() {
        this.portaAberta = true;
    }

    public void fecharPorta() {
        this.portaAberta = false;
    }

    public boolean isLigado() {
        return this.ligado;
    }

    public void ligar() {
        this.ligado = true;
        this.tempoDaUltimaAtualizacao = System.currentTimeMillis();
    }

    public void desligar() {
        this.ligado = false;
    }

    public boolean isTravado() {
        return this.travado;
    }

    public void travar() {
        this.travado = true;
    }

    public void destravar() {
        this.travado = false;
    }

}
