package microondasrp;

public class Display {

    public Display(){
        
    }
    
    public void ajustarTempoExibido(int minutos, int segundos){
        SimuladorMicroondas.getInstancia().ajustarTempo(minutos, segundos);
    }
    
    public void marcarPotenciaAlta(){
        SimuladorMicroondas.getInstancia().marcarPotenciaAlta();
    }
    
    public void marcarPotenciaMedia(){
        SimuladorMicroondas.getInstancia().marcarPotenciaMedia();
    }
    
    public void marcarPotenciaBaixa(){
        SimuladorMicroondas.getInstancia().marcarPotenciaBaixa();
    }
    
    public void iniciarCozimento(){
        SimuladorMicroondas.getInstancia().iniciarCozimento();
    }
    
    public void pararCozimento(){
        SimuladorMicroondas.getInstancia().pararCozimento();
    }
    
    public void abrirPorta(){
        SimuladorMicroondas.getInstancia().abrirPorta();
    }
    
    public void fecharPorta(){
        SimuladorMicroondas.getInstancia().fecharPorta();
    }
    
    public void desabilitarAbrirEFecharPorta(){
        SimuladorMicroondas.getInstancia().desabilitarJToggleButtonPorta();
    }
    
    public void reabilitarAbrirEFecharPorta(){
        SimuladorMicroondas.getInstancia().reabilitarJToggleButtonPorta();
    }
    
}
