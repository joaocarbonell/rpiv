package microondasrp;

public class PainelNumerico {

    public PainelNumerico() {

    }

    public void adicionar30Seg() {
        ControladorDoMicroondas.getInstancia().adicionarTempoDeCozimento(30);
    }

    public void reduzir30Seg() {
        ControladorDoMicroondas.getInstancia().reduzirTempoDeCozimento(30);
    }

    public void adicionar1Min() {
        ControladorDoMicroondas.getInstancia().adicionarTempoDeCozimento(60);
    }

    public void reduzir1Min() {
        ControladorDoMicroondas.getInstancia().reduzirTempoDeCozimento(60);
    }

    public void adicionar5Min() {
        ControladorDoMicroondas.getInstancia().adicionarTempoDeCozimento(300);
    }

    public void reduzir5Min() {
        ControladorDoMicroondas.getInstancia().reduzirTempoDeCozimento(300);
    }

    public void mudarParaPotenciaAlta() {
        ControladorDoMicroondas.getInstancia().escolherPotencia(Microondas.POTENCIA_ALTA);
    }

    public void mudarParaPotenciaMedia() {
        ControladorDoMicroondas.getInstancia().escolherPotencia(Microondas.POTENCIA_MEDIA);
    }

    public void mudarParaPotenciaBaixa() {
        ControladorDoMicroondas.getInstancia().escolherPotencia(Microondas.POTENCIA_BAIXA);
    }

    public void iniciarCozimento() {
        ControladorDoMicroondas.getInstancia().ligarCozimento();
    }

    public void pararCozimento() {
        ControladorDoMicroondas.getInstancia().desligarCozimento();
    }

    public void abrirPorta() {
        ControladorDoMicroondas.getInstancia().abrirPorta();
    }

    public void fecharPorta() {
        ControladorDoMicroondas.getInstancia().fecharPorta();
    }

    public void travarPainel() {
        ControladorDoMicroondas.getInstancia().travarPainel();
    }

    public void destravarPainel() {
        ControladorDoMicroondas.getInstancia().destravarPainel();
    }

}
