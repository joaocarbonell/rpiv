package microondasrp;

public interface IControlador {
    
    public Microondas getMicroondas();
    
    public void adicionarTempoDeCozimento(int tempo);
    public void reduzirTempoDeCozimento(int tempo);
    
    public void travarPainel();
    public void destravarPainel();
    
    public void ligarCozimento();
    public void desligarCozimento();
    
    public void abrirPorta();
    public void fecharPorta();
    
    public void escolherPotencia(int potencia);
    
}
