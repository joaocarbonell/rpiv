package microondasrp;


import java.io.File;
import java.io.IOException;
import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;

public class EfeitoDoSom {
    
    private File arquivo;
    private AudioInputStream fluxo;
    private AudioFormat formato;
    private Clip audio;
    
    public EfeitoDoSom(String caminho){
        //abre o arquivo de som e identifica seu formato
        arquivo = new File(caminho);
        try{
            fluxo = AudioSystem.getAudioInputStream(arquivo);
        }catch(UnsupportedAudioFileException ex){
            System.out.println("Audio da música não suportado");
        }catch(IOException e){
            System.out.println("Erro para abrir arquivo de música");
        }
        formato = fluxo.getFormat();
        
        //gera um clipe de áudio reproduzível e tenta deixá-lo aberto para reprodução
        DataLine.Info info = new DataLine.Info(Clip.class, formato);
        try {
            audio = (Clip) AudioSystem.getLine(info);
        } catch (LineUnavailableException ex) {
            System.out.println("Problema na carga da música");
        }
        try {
            audio.open(fluxo);
        } catch (LineUnavailableException ex) {
            System.out.println("Problema na abertura de fluxo para reprodução da música");
        } catch (IOException ex) {
            System.out.println("Problema na entrada e saída para reprodução da música");
        }
    }
    
    public void reproduzir(){
        //se estiver reproduzindo, interrompe
        if(audio.isRunning()){
            audio.stop();
        }
        //volta pro começo do arquivo e reproduz
        audio.setFramePosition(0);
        audio.start();
    }
    
}
