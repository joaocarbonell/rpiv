package microondasrp;

public class ControladorDeSom extends Thread {

    //para implementação do padrão singleton da classe
    private static ControladorDeSom instancia = null;

    private EfeitoDoSom[] sons = null;

    private ControladorDeSom() {
        this.sons = new EfeitoDoSom[3];
        sons[0] = new EfeitoDoSom("beep.wav");
        sons[1] = new EfeitoDoSom("laugh.wav");
        sons[1] = new EfeitoDoSom("scream.wav");

    }

    //obtém o controlado de som (cria um se não existir)
    public static ControladorDeSom getInstancia() {
        if (instancia == null) {
            instancia = new ControladorDeSom();
        }
        return instancia;
    }

    //reproduz um som
    public void reproduzir(String som) {
        if (som.equals("beep")) {
            sons[0].reproduzir();
        }
        if (som.equals("risada")) {
            sons[1].reproduzir();
        }
        if (som.equals("morte_joaninha")) {
            sons[2].reproduzir();
        }
    }

}
