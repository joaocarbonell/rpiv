/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package microondasrp;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author PC peter
 */
public class PainelNumericoTest {

    PainelNumerico painel;
    ControladorDoMicroondas controlador;

    public PainelNumericoTest() {
    }

    @Before
    public void setUp() {
        painel = new PainelNumerico();
        controlador = ControladorDoMicroondas.getInstancia();
    }

    @After
    public void tearDown() {
        painel = null;
        ControladorDoMicroondas.deleteInstancia();
    }

    /**
     * Test of adicionar30Seg method, of class PainelNumerico.
     */
    @Test
    public void testAdicionar30Seg() {
        painel.adicionar30Seg();
        assertEquals("Teste de Adicionar 30 segundos no cronometro.Cronometro zerado", 30, ControladorDoMicroondas.getInstancia().getMicroondas().getTempo());
    }

    /**
     * Test of reduzir30Seg method, of class PainelNumerico.
     */
    @Test
    public void testReduzir30Seg() {
        painel.adicionar30Seg();
        painel.reduzir30Seg();
        assertEquals("Teste de reduzir 30 segundos no cronometro.Cronometro com 30 segundos", 0, ControladorDoMicroondas.getInstancia().getMicroondas().getTempo());
    }

    @Test
    public void testReduzir30Seg2() {
        painel.adicionar1Min();
        painel.reduzir30Seg();
        assertEquals("Teste de reduzir 30 segundos no cronometro.Cronometro com 1 minuto", 30, ControladorDoMicroondas.getInstancia().getMicroondas().getTempo());
    }

    @Test
    public void testReduzir30Seg3() {
        painel.reduzir30Seg();
        assertEquals("Teste de reduzir 30 segundos no cronometro.Cronometro zerado", 0, ControladorDoMicroondas.getInstancia().getMicroondas().getTempo());
    }

    public void testAdicionar1Min() {
        painel.adicionar1Min();
        assertEquals("Teste de Adicionar 1 minuto no cronometro.Cronometro zerado", 60, ControladorDoMicroondas.getInstancia().getMicroondas().getTempo());
    }

    public void testAdicionar1Min2() {
        painel.adicionar5Min();
        painel.adicionar1Min();
        assertEquals("Teste de Adicionar 1 minuto no cronometro.Cronometro com 5 minutos", 360, ControladorDoMicroondas.getInstancia().getMicroondas().getTempo());
    }

    @Test
    public void testReduzir1Min() {
        painel.reduzir1Min();
        assertEquals("Teste de rezudir 1 minuto no cronometro.Cronometro zerado", 0, ControladorDoMicroondas.getInstancia().getMicroondas().getTempo());
    }

    @Test
    public void testReduzir1Min2() {
        painel.adicionar30Seg();
        painel.reduzir1Min();
        assertEquals("Teste de rezudir 1 minuto no cronometro.Cronometro com 30 segundos", 0, ControladorDoMicroondas.getInstancia().getMicroondas().getTempo());
    }

    @Test
    public void testReduzir1Min3() {
        painel.adicionar5Min();
        painel.reduzir1Min();
        assertEquals("Teste de rezudir 1 minuto no cronometro.Cronometro com 30 segundos", 240, ControladorDoMicroondas.getInstancia().getMicroondas().getTempo());
    }

    /**
     * Test of adicionar5Min method, of class PainelNumerico.
     */
    @Test
    public void testAdicionar5Min() {
        painel.adicionar5Min();
        assertEquals("Teste de Adicionar 5 minuto no cronometro.Cronometro com zerado", 300, ControladorDoMicroondas.getInstancia().getMicroondas().getTempo());
    }

    @Test
    public void testAdicionar5Min2() {
        painel.adicionar30Seg();
        painel.adicionar5Min();
        assertEquals("Teste de Adicionar 5 minuto no cronometro.Cronometro com 30 segundos", 330, ControladorDoMicroondas.getInstancia().getMicroondas().getTempo());
    }

    /**
     * Fizemos teste para verificar se o tempo pode passar de 99 minutos e 59
     * segundos.
     */
    @Test
    public void testAdicionar5Min3() {
        for (int i = 0; i <= 19; i++) {
            painel.adicionar5Min();
        }

        assertEquals("Teste de Adicionar 5 minuto no cronometro até alcançar 99 minutos e 59 segundos.Cronometro com zerado", 5999, ControladorDoMicroondas.getInstancia().getMicroondas().getTempo());
    }

    /**
     * Test of reduzir5Min method, of class PainelNumerico.
     */
    @Test
    public void testReduzir5Min() {
        painel.reduzir5Min();
        assertEquals("Teste de reduzir 5 minutos.Cronometro zerado", 0, ControladorDoMicroondas.getInstancia().getMicroondas().getTempo());
    }

    @Test
    public void testReduzir5Min2() {
        painel.adicionar5Min();
        painel.adicionar1Min();
        painel.adicionar30Seg();
        painel.reduzir5Min();
        assertEquals("Teste de reduzir 5 minutos.Cronometro com 6 minutos e 30 segundos", 90, ControladorDoMicroondas.getInstancia().getMicroondas().getTempo());
    }

    /**
     */
    @Test
    public void testMudarParaPotenciaAlta() {
        painel.mudarParaPotenciaAlta();
        assertEquals("Teste de mudar a potência para alta.", 1, ControladorDoMicroondas.getInstancia().getMicroondas().getPotencia());
    }

    /**
     * Test of mudarParaPotenciaMedia method, of class PainelNumerico.
     */
    @Test
    public void testMudarParaPotenciaMedia() {
        painel.mudarParaPotenciaMedia();
        assertEquals("Teste de mudar a potência para média.", 2, ControladorDoMicroondas.getInstancia().getMicroondas().getPotencia());
    }

    /**
     * Test of mudarParaPotenciaBaixa method, of class PainelNumerico.
     */
    @Test
    public void testMudarParaPotenciaBaixa() {
        painel.mudarParaPotenciaBaixa();
        assertEquals("Teste de mudar a potência para baixa.", 3, ControladorDoMicroondas.getInstancia().getMicroondas().getPotencia());
    }

    /**
     * Test of iniciarCozimento method, of class PainelNumerico.
     */
    @Test
    public void testIniciarCozimento() {
        painel.iniciarCozimento();
        assertEquals("Teste de iniciar o cozimento. Cronometro zerado", false, ControladorDoMicroondas.getInstancia().getMicroondas().isLigado());
    }

    @Test
    public void testIniciarCozimento2() {
        painel.adicionar30Seg();
        painel.iniciarCozimento();
        assertEquals("Teste de iniciar o cozimento. Cronometro com 30 segundos", true, ControladorDoMicroondas.getInstancia().getMicroondas().isLigado());
    }

    @Test
    public void testIniciarCozimento3() {
        painel.adicionar30Seg();
        painel.iniciarCozimento();
        assertEquals("Teste de iniciar o cozimento. Cronometro com 30 segundos", true, ControladorDoMicroondas.getInstancia().getMicroondas().isLigado());
        painel.reduzir1Min();
        assertEquals("Teste de iniciar o cozimento.Cronometro com 30 segundos reduzindo 1 minuto", false, ControladorDoMicroondas.getInstancia().getMicroondas().isLigado());

    }

    /**
     * Test of pararCozimento method, of class PainelNumerico.
     */
    @Test
    public void testPararCozimento() {
        painel.adicionar30Seg();
        painel.iniciarCozimento();
        painel.pararCozimento();
        assertEquals("Teste de parar o cozimento.Cronometro inicial com 30 segundos", false, ControladorDoMicroondas.getInstancia().getMicroondas().isLigado());
        int tempo = ControladorDoMicroondas.getInstancia().getMicroondas().getTempo();
        painel.iniciarCozimento();
        assertEquals("Teste de parar o cozimento e iniciar.Cronometro inicial com 30 segundos", tempo, ControladorDoMicroondas.getInstancia().getMicroondas().getTempo());
    }

    /**
     * Test of abrirPorta method, of class PainelNumerico.
     */
    @Test
    public void testAbrirPorta() {
        painel.adicionar30Seg();
        painel.iniciarCozimento();
        painel.abrirPorta();
        assertEquals("Teste de abrir a porta com microondas em cozimento.", false, ControladorDoMicroondas.getInstancia().getMicroondas().isPortaAberta());
    }

    @Test
    public void testAbrirPorta2() {
        painel.adicionar30Seg();
        painel.abrirPorta();
        assertEquals("Teste de abrir a porta com microondas parado .", true, ControladorDoMicroondas.getInstancia().getMicroondas().isPortaAberta());
    }

    /**
     * Test of fecharPorta method, of class PainelNumerico.
     */
    @Test
    public void testFecharPorta() {
        painel.adicionar30Seg();
        painel.fecharPorta();
        assertEquals("Teste de fechar a porta com microondas parado.", false, ControladorDoMicroondas.getInstancia().getMicroondas().isPortaAberta());
    }

    @Test
    public void testFecharPorta2() {
        painel.adicionar30Seg();
        painel.iniciarCozimento();
        painel.fecharPorta();
        assertEquals("Teste de fechar a porta com microondas em cozimento.", false, ControladorDoMicroondas.getInstancia().getMicroondas().isPortaAberta());
    }

    /**
     * Test of travarPainel method, of class PainelNumerico.
     */
    @Test
    public void testTravarPainel() {
        painel.travarPainel();
        assertEquals("Teste de travar o painel", true, ControladorDoMicroondas.getInstancia().getMicroondas().isTravado());

    }

    /**
     * Test of destravarPainel method, of class PainelNumerico.
     */
    @Test
    public void testDestravarPainel() {
        painel.destravarPainel();
        assertEquals("Teste de destravar o painel", false, ControladorDoMicroondas.getInstancia().getMicroondas().isTravado());

    }
}
