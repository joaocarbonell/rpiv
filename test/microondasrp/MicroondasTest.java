/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package microondasrp;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author PC peter
 */
public class MicroondasTest {

    Microondas micro;

    public MicroondasTest() {
    }

    @Before
    public void setUp() {
        micro = new Microondas();
    }

    @After
    public void tearDown() {
        micro = null;
    }

    /**
     * Test of run method, of class Microondas.
     */
    @Test
    public void testRun() {
        micro.setTempo(0);
        micro.ligar();

        assertEquals("Teste de lógica do if,com tempo zerado e microondas ligado ele decrementa o tempo em -1 segundo", 0, micro.getTempo());

    }

    @Test
    public void testRun2() {
        micro.setTempo(30);
        micro.desligar();
        assertEquals("Teste de lógica do if,com tempo 30 segundos e microondas desligado ele decrementa o tempo em -1 segundo", 30, micro.getTempo());

    }

    @Test
    public void testRun3() {
        micro.setTempo(30);
        micro.ligar();
        assertEquals("Teste de lógica do if,com tempo 30 segundos e microondas ligado ele decrementa o tempo em -1 segundo", 29, micro.getTempo());

    }

    @Test
    public void testAbrirPorta() {
        micro.abrirPorta();
        assertEquals("Teste de abrir a porta do microondas.Microondas desligado", true, micro.isPortaAberta());
    }

    /**
     * Test of fecharPorta method, of class Microondas.
     */
    @Test
    public void testFecharPorta() {
        micro.fecharPorta();
        assertEquals("Teste de fechar a porta do microondas.Microondas cozinhando", false, micro.isPortaAberta());

    }

    /**
     * Test of ligar method, of class Microondas.
     */
    @Test
    public void testLigar() {
        micro.ligar();
        assertEquals("Teste de ligar o microondas", true, micro.isLigado());
    }

    /**
     * Test of desligar method, of class Microondas.
     */
    @Test
    public void testDesligar() {
        micro.desligar();
        assertEquals("Teste de desligar o microondas", false, micro.isLigado());
    }

    @Test
    public void testTravar() {
        micro.travar();
        assertEquals("Teste de travar painel.", true, micro.isTravado());
    }

    /**
     * Test of destravar method, of class Microondas.
     */
    @Test
    public void testDestravar() {
        micro.destravar();
        assertEquals("Teste de destravar painel.", false, micro.isTravado());
    }

}
