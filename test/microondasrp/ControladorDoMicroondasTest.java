/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package microondasrp;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;

/**
 *
 * @author PC peter
 */
public class ControladorDoMicroondasTest {

    ControladorDoMicroondas controladorMicro;

    public ControladorDoMicroondasTest() {

    }

    @Before
    public void setUp() {
        controladorMicro = ControladorDoMicroondas.getInstancia();
    }

    @After
    public void tearDown() {
        ControladorDoMicroondas.deleteInstancia();
    }

    /**
     * Identificador Teste: TCP01 Fizemos teste de adicionar 30 segundos.
     */
    @Test
    public void testAdicionarTempoDeCozimento() {
        controladorMicro.adicionarTempoDeCozimento(30);
        assertEquals("Teste de adicionar 30 segundos no microondas", 30, controladorMicro.getMicroondas().getTempo());
    }

    /**
     * Identificador:TCP02 Fizemos teste de adicionar 1 minuto. Resultado
     *
     */
    @Test
    public void testAdicionarTempoDeCozimento2() {
        controladorMicro.adicionarTempoDeCozimento(60);
        assertEquals("Teste de adicionar 1 minuto no microondas", 60, controladorMicro.getMicroondas().getTempo());
    }

    /**
     * Identificador:TCP03 Fizemos teste de adicionar 5 minuto.
     */
    @Test
    public void testAdicionarTempoDeCozimento3() {
        controladorMicro.adicionarTempoDeCozimento(300);
        assertEquals("Teste de adicionar 5 minutos no microondas", 300, controladorMicro.getMicroondas().getTempo());
    }

    /**
     * Identificador:TCP04 Fizemos teste adicionar 1 min no cronometro e
     * decrementar 30 segundos.
     */
    public void testAdicionarTempoDeCozimento4() {
        controladorMicro.adicionarTempoDeCozimento(60);
        controladorMicro.reduzirTempoDeCozimento(30);
        assertEquals("Teste de adicionar 1 minuto no microondas e decrementar 30 segundos", 30, controladorMicro.getMicroondas().getTempo());
    }

    /**
     * Identificador: TCP05
     */
    @Test
    public void testAdicionarTempoDeCozimento5() {
        controladorMicro.adicionarTempoDeCozimento(30);
        controladorMicro.ligarCozimento();
        assertEquals("Teste de adicionar 30 segundos no microondas e iniciar o cozimento.", true, controladorMicro.getMicroondas().isLigado());
    }

    /**
     * Identificador : TCP06
     */
    @Test
    public void testPararCozimento() {
        ControladorDoMicroondas.getInstancia().adicionarTempoDeCozimento(10);
        ControladorDoMicroondas.getInstancia().ligarCozimento();

        try {
            System.out.println(controladorMicro.getMicroondas().getTempo());
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ControladorDoMicroondasTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        controladorMicro.desligarCozimento();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException ex) {
            Logger.getLogger(ControladorDoMicroondasTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        assertEquals("Micro ligado", false, controladorMicro.getMicroondas().isLigado());
        assertEquals("Tempo até parar cozimento", 5, controladorMicro.getMicroondas().getTempo());
    }

    /**
     * Identificador: TCN03
     */
    @Test
    public void testCozinhar1() {
        controladorMicro.ligarCozimento();
        assertEquals("Teste de iniciar o cozimento com o cronometro zerado.",false, ControladorDoMicroondas.getInstancia().getMicroondas().isLigado());
    }

    /**
     * Identificador: TCN04
     */
    @Test
    public void testCozinhar2() {
        controladorMicro.adicionarTempoDeCozimento(60);
        controladorMicro.ligarCozimento();
        controladorMicro.desligarCozimento();
        controladorMicro.desligarCozimento();
        assertEquals("Teste de adicionar 1 minuto, iniciar o cozimento e parar 2 vezes.Intenção Zerar Cronometro.", 0, ControladorDoMicroondas.getInstancia().getMicroondas().getTempo());
    }

    /**
     * Identificador:TCN06
     */
    @Test
    public void testTravar1() {
        SimuladorMicroondas simulador = SimuladorMicroondas.getInstancia();

        ControladorDoMicroondas.getInstancia().travarPainel();
        assertEquals("Testando painel travado", true, ControladorDoMicroondas.getInstancia().getMicroondas().isTravado());

        ControladorDoMicroondas.getInstancia().abrirPorta();
        assertEquals("Testando abrir porta com painel travado", false, ControladorDoMicroondas.getInstancia().getMicroondas().isPortaAberta());

    }

    /**
     * Teste de abrir a porta e ligar o microondas.
     */
    @Test
    public void testLigarCozimento() {
        controladorMicro.abrirPorta();
        controladorMicro.ligarCozimento();
        assertEquals("Teste de abrir a porta e ligar o microondas.", false, controladorMicro.getMicroondas().isLigado());
    }

    /**
     * Teste de desligar o cozimento do microondas.
     */
    @Test
    public void testDesligarCozimento() {
        controladorMicro.adicionarTempoDeCozimento(30);
        controladorMicro.ligarCozimento();
        controladorMicro.desligarCozimento();
        assertEquals("Teste de desligar microondas.", false, controladorMicro.getMicroondas().isLigado());
    }

    /**
     * Teste de alterar as potencias, para alta, média e baixa.
     */
    @Test
    public void testEscolherPotencia() {
        controladorMicro.escolherPotencia(Microondas.POTENCIA_ALTA);
        assertEquals("Teste de potência: Alta", 1, controladorMicro.getMicroondas().getPotencia());
        controladorMicro.escolherPotencia(Microondas.POTENCIA_MEDIA);
        assertEquals("Teste de potência: Media", 2, controladorMicro.getMicroondas().getPotencia());
        controladorMicro.escolherPotencia(Microondas.POTENCIA_BAIXA);
        assertEquals("Teste de potência: Baixa", 3, controladorMicro.getMicroondas().getPotencia());
    }

    /**
     * Identificador: TCN01 - Fizemos teste de reduzir 30 segundos com
     * cronometro zerado.
     */
    @Test
    public void testReduzirTempoDeCozimento() {
        controladorMicro.reduzirTempoDeCozimento(30);
        assertEquals("Teste de reduzir 30 segundos.Cronometro zerado", 0, controladorMicro.getMicroondas().getTempo());
        assertEquals("Teste de reduzir 30 segundos.Cozimento parado", false, controladorMicro.getMicroondas().isLigado());

    }

    /**
     * Fizemos teste de decrementar tempo com cronometro positivo.
     */
    @Test
    public void testReduzirTempoDeCozimento2() {
        controladorMicro.adicionarTempoDeCozimento(30);
        controladorMicro.reduzirTempoDeCozimento(60);
        assertEquals("Teste de adicionar 30 segundos e reduzir 1 minuto.Cronometro zerado", 0, controladorMicro.getMicroondas().getTempo());
        assertEquals("Teste de adicionar 30 segundos e reduzir 1 minuto.Cozimento Parado", false, controladorMicro.getMicroondas().isLigado());
    }

    /**
     * Fizemos teste de abrir porta com microondas cozinhando.
     */
    @Test
    public void testAbrirPorta() {
        controladorMicro.ligarCozimento();
        controladorMicro.abrirPorta();
        assertEquals("Teste abrir porta com microondas cozinhando.", false, controladorMicro.getMicroondas().isPortaAberta());
    }

    /**
     *
     */
    @Test
    public void testAbrirPorta2() {
        controladorMicro.ligarCozimento();
        controladorMicro.desligarCozimento();
        controladorMicro.abrirPorta();
        assertEquals("Teste abrir porta com microondas parado.", true, controladorMicro.getMicroondas().isPortaAberta());
    }

    /**
     * Fizemos teste de fechar a porta com o microondas cozimenhando.
     */
    @Test
    public void testFecharPorta() {
        controladorMicro.ligarCozimento();
        controladorMicro.fecharPorta();
        assertEquals("Teste fechar porta com microondas cozinhando.", false, controladorMicro.getMicroondas().isPortaAberta());
    }

    @Test
    public void testFecharPorta2() {
        controladorMicro.ligarCozimento();
        controladorMicro.desligarCozimento();
        controladorMicro.fecharPorta();
        assertEquals("Teste fechar porta com microondas parado.", false, controladorMicro.getMicroondas().isPortaAberta());
    }

}
